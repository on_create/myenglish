CREATE TABLE Dictionary_db (
  _id INTEGER PRIMARY KEY AUTOINCREMENT,
  word_en TEXT NOT NULL,
  transcription TEXT NOT NULL,
  translate_ru TEXT NOT NULL,
  category TEXT NOT NULL,
  progress_collect INTEGER NOT NULL,
  progress_card_ru INTEGER NOT NULL,
  progress_card_en INTEGER NOT NULL
);

CREATE TABLE Irregular_Verbs_db (
  _id INTEGER PRIMARY KEY AUTOINCREMENT,
  infinitive TEXT NOT NULL,
  infinitive_tr TEXT NOT NULL,
  past_simple TEXT NOT NULL,
  past_simple_tr TEXT NOT NULL,
  past_participle TEXT NOT NULL,
  past_participle_tr TEXT NOT NULL,
  translate_verb_ru TEXT NOT NULL,
  progress_speed INTEGER NOT NULL
);