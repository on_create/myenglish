package com.dvoyka.myenglish;

import android.content.Context;

import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class Application extends android.app.Application {

    private static final int ONE_MB = 1024, ONE_H = 100, FIVE = 5, TWO = 2;

    @Override
    public void onCreate() {
        super.onCreate();
        initImageLoader(getApplicationContext());
    }

    public static void initImageLoader(Context context) {

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(ONE_H, ONE_H)
                .diskCacheExtraOptions(ONE_H, ONE_H, null)
                .threadPoolSize(FIVE)
                .threadPriority(Thread.MIN_PRIORITY + TWO)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new UsingFreqLimitedMemoryCache(TWO * ONE_MB * ONE_MB))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();
        ImageLoader.getInstance().init(config);
    }

}
