package com.dvoyka.myenglish.helper;

import android.content.Context;
import android.os.AsyncTask;

import com.dvoyka.myenglish.db.BaseDB;


public class UpdateMyDictionary extends AsyncTask<Void, Void, Void> {

    private BaseDB db;
    private Context mycontext;

    UpdateMyDictionary(Context context){
        this.mycontext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        db = new BaseDB(mycontext);
        db.open();
    }

    @Override
    protected Void doInBackground(Void... voids) {

        db.beginTransaction();
        try {
            
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
