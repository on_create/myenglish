package com.dvoyka.myenglish.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "dictionary.db";
    private static final int DB_VERSION = 1;
    public Context ctx;

    public static final String DB_TABLE_DICTIONARY = "Dictionary_db";
    public static final String WORD_EN = "word_en";
    public static final String TRANSCRIPTION = "transcription";
    public static final String WORD_RU = "translate_ru";
    public static final String CATEGORY = "category";
    public static final String PROGRESS_COLLECT_WORD = "progress_collect";
    public static final String PROGRESS_CARD_RU = "progress_card_ru";
    public static final String PROGRESS_CARD_EN = "progress_card_en";

    public static final String DB_TABLE_VERBS = "Irregular_Verbs_db";
    public static final String WORD_EN_I_VERBS = "infinitive";
    public static final String TRANSCRIPTION_I_VERBS = "infinitive_tr";
    public static final String WORD_EN_PS_VERBS = "past_simple";
    public static final String TRANSCRIPTION_PS_VERBS = "past_simple_tr";
    public static final String WORD_EN_PP_VERBS = "past_participle";
    public static final String TRANSCRIPTION_PP_VERBS = "past_participle_tr";
    public static final String WORD_RU_VERBS = "translate_verb_ru";
    public static final String PROGRESS_SPEED = "progress_speed";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.ctx = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        new CreateDB(ctx, sqLiteDatabase).execute();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
