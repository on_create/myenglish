package com.dvoyka.myenglish.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class BaseDB  {

    private Context context;
    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;
    private final String PR_COLLECT = "progress_collect < 10", PR_CARD_RU = "progress_card_ru < 10", PR_CARD_EN = "progress_card_en < 10",
    CATEGORY_NAME = "my dictionary";

    public BaseDB(Context context) {
        this.context = context;
    }

    public BaseDB open() throws SQLException {
        mDBHelper = new DBHelper(context);
        this.mDB = this.mDBHelper.getReadableDatabase();
        return this;
    }

    public void beginTransaction(){
        mDB.beginTransaction();
    }

    public void setTransactionSuccessful(){
        mDB.setTransactionSuccessful();
    }

    public void endTransaction(){
        mDB.endTransaction();
    }

    public void close() {
        if (this.mDBHelper != null) {
            this.mDBHelper.close();
        }
    }



    public Cursor getAllDataDictionary() {
        return mDB.query(DBHelper.DB_TABLE_DICTIONARY, null, null, null, null, null, null);
    }

    public Cursor getRandomWordCollect(){
        return mDB.query(DBHelper.DB_TABLE_DICTIONARY, null, PR_COLLECT, null, null, null, null);
    }

    public Cursor getRandomWordCardRu(){
        return mDB.query(DBHelper.DB_TABLE_DICTIONARY, null, PR_CARD_RU, null, null, null, null);
    }

    public Cursor getRandomWordCardEn(){
        return mDB.query(DBHelper.DB_TABLE_DICTIONARY, null, PR_CARD_EN, null, null, null, null);
    }

    public Cursor getAllDataVerbs() {
        return mDB.query(DBHelper.DB_TABLE_VERBS, null, null, null, null, null, null);
    }

    public void addWord(String word, String transcription, String translate){
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.WORD_EN, word);
        cv.put(DBHelper.TRANSCRIPTION, transcription);
        cv.put(DBHelper.WORD_RU, translate);
        cv.put(DBHelper.CATEGORY, CATEGORY_NAME);
        cv.put(DBHelper.PROGRESS_COLLECT_WORD, 0);
        cv.put(DBHelper.PROGRESS_CARD_EN, 0);
        cv.put(DBHelper.PROGRESS_CARD_RU, 0);
        mDB.insert(DBHelper.DB_TABLE_DICTIONARY, null, cv);
    }
}
