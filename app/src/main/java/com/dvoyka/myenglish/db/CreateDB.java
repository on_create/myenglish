package com.dvoyka.myenglish.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.XmlRes;
import android.text.TextUtils;
import android.widget.Toast;

import com.dvoyka.myenglish.R;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Scanner;


public class CreateDB extends AsyncTask<Void, Void, Void> {

    private final String RECORD = "record";
    private Context ctx;
    private SQLiteDatabase sqLiteDatabase;
    private final String FILE_NAME = "create_dictionarys.sql";
    enum DataType {WORDS, VERBS}

    CreateDB(Context context, SQLiteDatabase sqLiteDatabase){
        this.ctx = context;
        this.sqLiteDatabase = sqLiteDatabase;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {

        processFile(sqLiteDatabase, FILE_NAME);

        putData(sqLiteDatabase, R.xml.dictionary, DataType.WORDS);
        putData(sqLiteDatabase, R.xml.verbs, DataType.VERBS);

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private void processFile(SQLiteDatabase db, String file) {
        String next;
        Scanner scanner = null;
        try {
            scanner = new Scanner(ctx.getAssets().open(file));
            scanner.useDelimiter(";");

            while (scanner.hasNext()) {
                next = scanner.next().trim();
                if (!TextUtils.isEmpty(next)) {
                    db.execSQL(next);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (scanner != null) {
                    scanner.close();
                }
            } catch (Exception ignored) {
            }
        }
    }

    public void putData(SQLiteDatabase db, @XmlRes int resId, DataType datatype){
        ContentValues cv = new ContentValues();
        XmlResourceParser _xml = ctx.getResources().getXml(resId);

        try {
            for (int eventType = _xml.getEventType(); eventType != _xml.END_DOCUMENT; eventType = _xml.next()) {
                if (eventType == _xml.START_TAG) {
                    if (_xml.getName().equals(RECORD)) {
                        switch (datatype){
                            case WORDS:
                                String image = _xml.getAttributeValue(0);
                                String word = _xml.getAttributeValue(1);
                                String translate = _xml.getAttributeValue(2);
                                String category = _xml.getAttributeValue(3);
                                String included = _xml.getAttributeValue(4);
                                String transcription = _xml.getAttributeValue(5);
                                cv.put(DBHelper.WORD_EN, word);
                                cv.put(DBHelper.TRANSCRIPTION, transcription);
                                cv.put(DBHelper.WORD_RU, translate);
                                cv.put(DBHelper.CATEGORY, category);
                                cv.put(DBHelper.PROGRESS_COLLECT_WORD, 0);
                                cv.put(DBHelper.PROGRESS_CARD_EN, 0);
                                cv.put(DBHelper.PROGRESS_CARD_RU, 0);
                                db.insert(DBHelper.DB_TABLE_DICTIONARY, null, cv);
                                break;
                            case VERBS:
                                String inf = _xml.getAttributeValue(0);
                                String infTran = _xml.getAttributeValue(1);
                                String ps = _xml.getAttributeValue(2);
                                String psTran = _xml.getAttributeValue(3);
                                String pp = _xml.getAttributeValue(4);
                                String ppTran = _xml.getAttributeValue(5);
                                String transl = _xml.getAttributeValue(6);
                                cv.put(DBHelper.WORD_EN_I_VERBS, inf);
                                cv.put(DBHelper.TRANSCRIPTION_I_VERBS, infTran);
                                cv.put(DBHelper.WORD_EN_PS_VERBS, ps);
                                cv.put(DBHelper.TRANSCRIPTION_PS_VERBS, psTran);
                                cv.put(DBHelper.WORD_EN_PP_VERBS, pp);
                                cv.put(DBHelper.TRANSCRIPTION_PP_VERBS, ppTran);
                                cv.put(DBHelper.WORD_RU_VERBS, transl);
                                cv.put(DBHelper.PROGRESS_SPEED, 0);
                                db.insert(DBHelper.DB_TABLE_VERBS, null, cv);
                                break;
                        }
                    }
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } finally {
            _xml.close();
        }
    }
}
