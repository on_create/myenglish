package com.dvoyka.myenglish.screens.dictionarys;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.adapter.DictionaryPagerAdapter;
import com.dvoyka.myenglish.db.BaseDB;
import com.dvoyka.myenglish.db.DBHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class FragmDictionarys extends Fragment  implements LoaderManager.LoaderCallbacks<Cursor> {

    private ViewPager mViewPager;
    private DictionaryPagerAdapter dictionaryAdapter;

    private BaseDB db;

    private ArrayList<Word> list_dict_00, list_dict_01, list_dict_02, list_dict_03, list_dict_04, list_dict_05,
            list_dict_06, list_dict_07, list_dict_08, list_dict_09;
    private HashMap<String, Integer> keyCategory = new HashMap<>();
    private String[] nameCategory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_dictionarys, container, false);

        nameCategory = getActivity().getResources().getStringArray(R.array.namecategory);
        for(int i = 0; i < nameCategory.length; i++){
            keyCategory.put(nameCategory[i], i);
        }

        db = new BaseDB(getActivity());
        db.open();

        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);

        getActivity().getSupportLoaderManager().initLoader(0, null, this);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new DataLoaderDictionary(getActivity(), db);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        new ShowDictionarys().execute(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    static class DataLoaderDictionary extends CursorLoader {
        BaseDB db;
        public DataLoaderDictionary(Context context, BaseDB db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getAllDataDictionary();
            return cursor;
        }
    }

    private class ShowDictionarys extends AsyncTask<Cursor, Void, ArrayList>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList doInBackground(Cursor... cursors) {
            return getListsFromCursor(cursors[0]);
        }

        @Override
        protected void onPostExecute(ArrayList arrayList) {
            super.onPostExecute(arrayList);

            dictionaryAdapter = new DictionaryPagerAdapter(getChildFragmentManager(), getActivity(), arrayList);
            mViewPager.setAdapter(dictionaryAdapter);
        }
    }

    ArrayList<ArrayList> getListsFromCursor (Cursor data){

        list_dict_00 = new ArrayList<>();
        list_dict_01 = new ArrayList<>();
        list_dict_02 = new ArrayList<>();
        list_dict_03 = new ArrayList<>();
        list_dict_04 = new ArrayList<>();
        list_dict_05 = new ArrayList<>();
        list_dict_06 = new ArrayList<>();
        list_dict_07 = new ArrayList<>();
        list_dict_08 = new ArrayList<>();
        list_dict_09 = new ArrayList<>();

        ArrayList<ArrayList> listDict = new ArrayList<>();
        data.moveToFirst();
        while (data.moveToNext()){
            switch (keyCategory.get(data.getString(data.getColumnIndex(DBHelper.CATEGORY)))){
                case 0:
                    list_dict_00.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 1:
                    list_dict_01.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 2:
                    list_dict_02.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 3:
                    list_dict_03.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 4:
                    list_dict_04.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 5:
                    list_dict_05.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 6:
                    list_dict_06.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 7:
                    list_dict_07.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 8:
                    list_dict_08.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
                case 9:
                    list_dict_09.add(new Word(data.getString(data.getColumnIndex(DBHelper.WORD_EN)),
                            data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION)),
                            data.getString(data.getColumnIndex(DBHelper.WORD_RU))));
                    break;
            }
        }
        listDict.add(list_dict_00);
        listDict.add(list_dict_01);
        listDict.add(list_dict_02);
        listDict.add(list_dict_03);
        listDict.add(list_dict_04);
        listDict.add(list_dict_05);
        listDict.add(list_dict_06);
        listDict.add(list_dict_07);
        listDict.add(list_dict_08);
        listDict.add(list_dict_09);

        return listDict;
    }

}
