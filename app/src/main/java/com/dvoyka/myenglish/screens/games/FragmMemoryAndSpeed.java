package com.dvoyka.myenglish.screens.games;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.dvoyka.myenglish.FirstActivity;
import com.dvoyka.myenglish.FirstActivity.Game;
import com.dvoyka.myenglish.R;

public class FragmMemoryAndSpeed extends Fragment {

    private Display display;
    private Point size;
    private int indentTop, indentLeft, weightCell;
    private int NUMBER_CELLS = 5;
    private FrameLayout flGameFild;
    private GameFieldMemory drawGameFieldMemory;
    private GameFieldSpeed drawGameFieldSpeep;
    private Game game;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_memory_and_speed, container, false);

        display = getActivity().getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        game = (Game) getArguments().getSerializable(getString(R.string.training));

        flGameFild = (FrameLayout)view.findViewById(R.id.gameField);

        showFragmentContent(game, flGameFild);

        return view;
    }

    void showFragmentContent(Game game, FrameLayout view){
        switch (game){
            case MEMORY:

                indentLeft = (size.x/15)/2;
                indentTop = ((size.y - size.x)/2) - indentLeft;
                weightCell = (size.x - indentLeft*2)/NUMBER_CELLS;

                drawGameFieldMemory = new GameFieldMemory(getActivity(), weightCell, indentTop, indentLeft, NUMBER_CELLS);
                view.addView(drawGameFieldMemory);
                break;
            case SPEED:
                drawGameFieldSpeep = new GameFieldSpeed(getActivity());
                view.addView(drawGameFieldSpeep);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        switch (game){
            case MEMORY:
                drawGameFieldMemory.resume();
                break;
            case SPEED:
                drawGameFieldSpeep.resume();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        switch (game){
            case MEMORY:
                drawGameFieldMemory.pause();
                break;
            case SPEED:
                drawGameFieldSpeep.pause();
                break;
        }
    }
}
