package com.dvoyka.myenglish.screens.dictionarys;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.adapter.VerbsPagerAdapter;
import com.dvoyka.myenglish.db.BaseDB;
import com.dvoyka.myenglish.db.DBHelper;

import java.util.ArrayList;

public class FragmVerbs extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private ViewPager mViewPager;
    private VerbsPagerAdapter verbsPagerAdapter;
    private BaseDB db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_verbs, container, false);

        db = new BaseDB(getActivity());
        db.open();

        mViewPager = (ViewPager) view.findViewById(R.id.viewpagerVerbs);

        getActivity().getSupportLoaderManager().initLoader(1, null, this);

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new DataLoaderVerbs(getActivity(), db);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        new ShowVerbs().execute(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    static class DataLoaderVerbs extends CursorLoader {
        BaseDB db;
        public DataLoaderVerbs(Context context, BaseDB db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getAllDataVerbs();
            return cursor;
        }
    }

    private class ShowVerbs extends AsyncTask<Cursor, Void, ArrayList> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList doInBackground(Cursor... cursors) {
            return getListFromCursor(cursors[0]);
        }

        @Override
        protected void onPostExecute(ArrayList arrayList) {
            super.onPostExecute(arrayList);

            verbsPagerAdapter = new VerbsPagerAdapter(getChildFragmentManager(), getActivity(), arrayList);
            mViewPager.setAdapter(verbsPagerAdapter);
        }
    }

    ArrayList<Verb> getListFromCursor(Cursor data){
        ArrayList<Verb> list = new ArrayList<>();
        data.moveToFirst();
        do{
            list.add(new Verb(data.getString(data.getColumnIndex(DBHelper.WORD_EN_I_VERBS)),
                    data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION_I_VERBS)),
                    data.getString(data.getColumnIndex(DBHelper.WORD_EN_PS_VERBS)),
                    data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION_PS_VERBS)),
                    data.getString(data.getColumnIndex(DBHelper.WORD_EN_PP_VERBS)),
                    data.getString(data.getColumnIndex(DBHelper.TRANSCRIPTION_PP_VERBS)),
                    data.getString(data.getColumnIndex(DBHelper.WORD_RU_VERBS))));
        } while (data.moveToNext());

        return list;
    }
}
