package com.dvoyka.myenglish.screens.dictionarys;

public class Verb {

    private String wordI;
    private String transcriptionI;
    private String wordPS;
    private String transcriptionPS;
    private String wordPP;
    private String transcriptionPP;
    private String translate;

    Verb(String wordI, String transcriptionI, String wordPS, String transcriptionPS, String wordPP, String transcriptionPP, String translate){
        this.wordI = wordI;
        this.transcriptionI = transcriptionI;
        this.wordPS = wordPS;
        this.transcriptionPS = transcriptionPS;
        this.wordPP = wordPP;
        this.transcriptionPP = transcriptionPP;
        this.translate = translate;
    }

    public String getWordI(){
        return wordI;
    }

    public String getTranscriptionI(){
        return transcriptionI;
    }

    public String getWordPS(){
        return wordPS;
    }

    public String getTranscriptionPS(){
        return transcriptionPS;
    }

    public String getWordPP(){
        return wordPP;
    }

    public String getTranscriptionPP(){
        return transcriptionPP;
    }

    public String getTranslate(){
        return translate;
    }

}
