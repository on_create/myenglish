package com.dvoyka.myenglish.screens.games;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.dvoyka.myenglish.R;

public class GameFieldMemory extends SurfaceView implements Runnable {

    private Context mycontext;
    private Thread thread = null;
    private SurfaceHolder holder;
    volatile boolean isOK = false;
    private int weightCell, indentTop, indentLeft, xTouch, yTouch;
    private Paint paintGameField, paintCell;
    private SpriteMemory[][] sprite;
    private boolean winGame = false;
    private int numberCount = 3;

    private Paint p = new Paint();

    private final int REDRAW_TIME    = 40;
    private long    mPrevRedrawTime;
    private int stepAnimDown, stepAnimUp;
    private int[] stepsAnimation = { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 16, 17, 17};


    public GameFieldMemory(Context context, int weightCell, int indentTop, int indentLeft, int numberCount) {
        super(context);
        holder = getHolder();
        this.mycontext = context;
        this.weightCell = weightCell;
        this.indentTop = indentTop;
        this.indentLeft = indentLeft;
        this.numberCount = numberCount;

        stepAnimUp = stepsAnimation.length-1;

        p.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        sprite = new SpriteMemory[numberCount][numberCount];

        paintCell = new Paint();
        paintGameField = new Paint();
        paintGameField.setColor(ContextCompat.getColor(mycontext, R.color.colorGameField));

        new AddSpriteForNewGame();
    }

    public long getTime() {
        return System.nanoTime() / 1000000;
    }

    @Override
    public void run() {

        while (isOK) {
            long curTime = getTime();
            long elapsedTime = curTime - mPrevRedrawTime;
            if (elapsedTime < REDRAW_TIME)
                continue;

            if (!holder.getSurface().isValid())
                continue;
            Canvas canvas = holder.lockCanvas();
            drawing(canvas);
            holder.unlockCanvasAndPost(canvas);

            mPrevRedrawTime = curTime;
        }
    }


    public void drawing(Canvas canvas){

        canvas.drawColor(ContextCompat.getColor(mycontext, R.color.colorBackground));
        canvas.drawRoundRect(new RectF(indentLeft, indentTop, indentLeft + weightCell * numberCount, indentTop + weightCell * numberCount), 4, 4, paintGameField);

        for(int i = 1; i <= numberCount-1; i++){
            canvas.drawLine( (i * weightCell) + indentLeft, indentTop, (i * weightCell) + indentLeft, indentTop + weightCell * numberCount, p);
        }
        for(int i = 1; i <= numberCount-1; i++){
            canvas.drawLine( indentLeft, (i * weightCell)+indentTop, indentLeft + weightCell * numberCount, (i * weightCell)+indentTop, p);
        }

        drawSprite(canvas);

    }

    void drawSprite(Canvas canvas){
        for (int i = 0; i < numberCount; i++) {
            for (int j = 0; j < numberCount; j++) {
                if (sprite[i][j] != null) {
                    if(sprite[i][j].getStepAnimDown() < stepsAnimation.length){
                        sprite[i][j].setStepAnimDown(sprite[i][j].getStepAnimDown()+1);
                        int step = sprite[i][j].getStepAnimDown()-1;
                        sprite[i][j].draw(canvas, paintCell, weightCell, i * weightCell, j * weightCell, stepsAnimation[step]);
                    }else{
                        sprite[i][j].setStepAnimUp(sprite[i][j].getStepAnimUp()-1);
                        int step = sprite[i][j].getStepAnimUp();
                        sprite[i][j].draw(canvas, paintCell, weightCell, i * weightCell, j * weightCell, stepsAnimation[step]);
                        if(sprite[i][j].getStepAnimUp() == 0){
                            sprite[i][j].setStepAnimDown(0);
                            sprite[i][j].setStepAnimUp(stepsAnimation.length-1);
                        }
                    }
                }
            }
        }
    }


    void addSpriteToField(){

        for (int i = 0; i < numberCount; i++) {
            if(i%2==0){
                stepAnimDown = 0;
                stepAnimUp = stepsAnimation.length-1;
            }else{
                stepAnimDown = stepsAnimation.length-1;
                stepAnimUp = stepsAnimation.length-1;
            }
            for (int j = 0; j < numberCount; j++) {
                sprite[i][j] = new SpriteMemory( mycontext, ContextCompat.getColor(mycontext, R.color.colorBlackCell), indentLeft, indentTop, stepAnimUp, stepAnimDown);
            }
        }
    }

    class AddSpriteForNewGame implements Runnable {

        Thread threadAdd;

        AddSpriteForNewGame() {
            threadAdd = new Thread(this);
            threadAdd.start();
        }

        public void run() {

            for (int i = 0; i < numberCount; i++) {
                if(i%2==0){
                    stepAnimDown = 0;
                    stepAnimUp = stepsAnimation.length-1;
                }else{
                    stepAnimDown = stepsAnimation.length-1;
                    stepAnimUp = stepsAnimation.length-1;
                }
                for (int j = 0; j < numberCount; j++) {
                    sprite[i][j] = new SpriteMemory( mycontext, ContextCompat.getColor(mycontext, R.color.colorBlackCell), indentLeft, indentTop, stepAnimUp, stepAnimDown);
                    try {
                        threadAdd.sleep(60);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        xTouch = (int)((event.getX()-indentLeft)/weightCell);
        yTouch = (int)((event.getY()-indentTop)/weightCell);

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (event.getY() > indentTop && indentTop + weightCell * numberCount > event.getY()
                        && event.getX() > indentLeft && event.getX() < indentLeft + weightCell * numberCount) {
                    sprite[xTouch][yTouch] = null;
                }
                return true;
        }
        return false;
    }

    public void pause() {
        isOK = false;
        while (true) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;
        }
        thread = null;
    }

    public void resume() {
        isOK = true;
        thread = new Thread(this);
        thread.start();
    }

}
