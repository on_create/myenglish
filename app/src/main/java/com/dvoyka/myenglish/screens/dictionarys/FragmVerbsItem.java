package com.dvoyka.myenglish.screens.dictionarys;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dvoyka.myenglish.R;

public class FragmVerbsItem extends Fragment implements View.OnClickListener {

    private String wordInf, wordPS, wordPP, transcriptionInf, transcriptionPS, transcriptionPP, translate;
    private Button buttonCheck;
    private boolean check = false;
    private LinearLayout layoutShow, layoutCheck;
    private EditText editTextI, editTextPS, editTextPP;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.item_verbs, container, false);

        layoutShow = (LinearLayout)view.findViewById(R.id.containerShowVerbs);
        layoutCheck = (LinearLayout)view.findViewById(R.id.containerCheckVerbs);

        buttonCheck = (Button)view.findViewById(R.id.buttonCheckVerbs);
        buttonCheck.setOnClickListener(this);

        editTextI = (EditText)view.findViewById(R.id.editTextCheckVerbsInf);
        editTextPS = (EditText)view.findViewById(R.id.editTextCheckVerbsPS);
        editTextPP = (EditText)view.findViewById(R.id.editTextCheckVerbsPP);

        Bundle arguments = getArguments();
        if (arguments != null) {
            wordInf = arguments.getString(getString(R.string.word_inf));
            wordPS = arguments.getString(getString(R.string.word_ps));
            wordPP = arguments.getString(getString(R.string.word_pp));
            transcriptionInf = arguments.getString(getString(R.string.transcription_inf));
            transcriptionPS = arguments.getString(getString(R.string.transcription_ps));
            transcriptionPP = arguments.getString(getString(R.string.transcription_pp));
            translate = arguments.getString(getString(R.string.translate));

            displayContent(view, wordInf, wordPS, wordPP, transcriptionInf, transcriptionPS, transcriptionPP, translate);
        }

        return view;
    }

    private void displayContent(View v, String wordI, String wordPS, String wordPP,
                                String transcriptionI, String transcriptionPS, String transcriptionPP, String translate) {
        TextView tvWordI = (TextView) v.findViewById(R.id.tvinfinitive_word);
        TextView tvTranscriptionI = (TextView) v.findViewById(R.id.tvinfinitive_transcription);
        tvWordI.setText(wordI);
        tvTranscriptionI.setText(transcriptionI);

        TextView tvWordPS = (TextView) v.findViewById(R.id.tvpast_simple_word);
        TextView tvTranscriptionPS = (TextView) v.findViewById(R.id.tvpast_simple_transcription);
        tvWordPS.setText(wordPS);
        tvTranscriptionPS.setText(transcriptionPS);

        TextView tvWordPP = (TextView) v.findViewById(R.id.tvpast_participle_word);
        TextView tvTranscriptionPP = (TextView) v.findViewById(R.id.tvpast_participle_transcription);
        tvWordPP.setText(wordPP);
        tvTranscriptionPP.setText(transcriptionPP);

        TextView tvTransl = (TextView) v.findViewById(R.id.tv_translate_verb);
        tvTransl.setText(translate);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonCheckVerbs:
                if(check){
                    checkAnswer();
                }else{
                    buttonCheck.setText(getString(R.string.check_all_verbs));
                    check = true;
                    layoutShow.setVisibility(View.GONE);
                    layoutCheck.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    void checkAnswer(){
        Toast.makeText(getActivity(), editTextI.getText()+" "+wordInf+" "+editTextI.getText().toString().equals(wordInf), Toast.LENGTH_SHORT).show();
        if(editTextI.getText().toString().equals(wordInf) && editTextPS.getText().toString().equals(wordPS) && editTextPP.getText().toString().equals(wordPP))
            Toast.makeText(getActivity(), "ok", Toast.LENGTH_SHORT).show();
    }
}
