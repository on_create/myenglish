package com.dvoyka.myenglish.screens.games;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.dvoyka.myenglish.R;

public class GameFieldSpeed extends SurfaceView implements Runnable {

    private Context context;
    private Thread thread = null;
    private SurfaceHolder holder;
    volatile boolean isOK = false;
    private final int REDRAW_TIME = 30;
    private long mPrevRedrawTime;

    public GameFieldSpeed(Context context) {
        super(context);
        holder = getHolder();
        this.context = context;
    }

    public long getTime() {
        return System.nanoTime() / 1000000;
    }

    @Override
    public void run() {
        while (isOK) {
            long curTime = getTime();
            long elapsedTime = curTime - mPrevRedrawTime;
            if (elapsedTime < REDRAW_TIME)
                continue;

            if (!holder.getSurface().isValid())
                continue;
            Canvas canvas = holder.lockCanvas();
            drawing(canvas);
            holder.unlockCanvasAndPost(canvas);

            mPrevRedrawTime = curTime;
        }
    }

    public void drawing(Canvas canvas){

        canvas.drawColor(ContextCompat.getColor(context, R.color.colorBackground));

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                return true;
        }
        return false;
    }

    public void pause() {
        isOK = false;
        while (true) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;
        }
        thread = null;
    }

    public void resume() {
        isOK = true;
        thread = new Thread(this);
        thread.start();
    }
}
