package com.dvoyka.myenglish.screens.dictionarys;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.adapter.DictionaryListAdapter;

import java.util.ArrayList;

public class FragmDictionarysContent extends Fragment {

    public static final String WORD = "word";
    public static final String TRANSCRIPTION = "transcription";
    public final static String TRANSLATE = "translate";

    private ListView listView;
    private ArrayList<Word> listWord = new ArrayList<>();
    private TextView textViewNoData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_dictionarys_content, container, false);

        listView = (ListView)view.findViewById(R.id.listViewDictionary);
        listWord = (ArrayList<Word>) getArguments().getSerializable(getString(R.string.array_object));

        if(listWord.size() > 0){
            DictionaryListAdapter adapter = new DictionaryListAdapter(getActivity(), R.layout.item_dictionary, listWord);
            listView.setAdapter(adapter);
        }else{
            textViewNoData = (TextView)view.findViewById(R.id.textViewEmpty);
            textViewNoData.setVisibility(View.VISIBLE);
        }

        return view;
    }

}
