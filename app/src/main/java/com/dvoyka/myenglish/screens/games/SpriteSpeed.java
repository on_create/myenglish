package com.dvoyka.myenglish.screens.games;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;

import com.dvoyka.myenglish.R;

public class SpriteSpeed {

    private int color;
    private int x=50, y=0;
    private int widht, height, heightField, step;
    private String word;
    private Paint paint;
    private Context context;

    public SpriteSpeed(Context context, int colorSprite, int x, int y, int widht, int height, int heightField, int step, Paint p){
        this.context = context;
        this.color = colorSprite;
        this.x = x;
        this.y = y;
        this.widht = widht;
        this.height = height;
        this.heightField = heightField;
        this.step = step;
        this.paint = p;

        paint.setColor(color);
        paint.setShadowLayer(5.0f, 1.0f, 1.0f, ContextCompat.getColor(context, R.color.colorShadow));
    }


    public void setColorSprite (int colorSprite){
        this.color = colorSprite;
    }

    public int getColorSprite(){
        return color;
    }

    public void setSpriteX(int x){
        this.x = x;
    }

    public int getSpriteX(){
        return x;
    }

    public void setSpriteY(int y){
        this.y = y;
    }

    public int getSpriteY(){
        return y;
    }

    public String getWord(){
        return word;
    }

    public void setWord(String word){
        this.word = word;
    }

    public void draw(Canvas canvas) {
        canvas.drawRoundRect(new RectF( x, y, (x + widht), y + height), 5, 5, paint);
        if(y < heightField)
            y=y+step;
    }
}
