package com.dvoyka.myenglish.screens.dictionarys;


public class Word {

    private String word;
    private String transcription;
    private String translate;

    Word(String inWord, String inTranscrition, String inTranslate){
        this.word = inWord;
        this.transcription = inTranscrition;
        this.translate = inTranslate;
    }

    void setAllData(String inWord, String inTranscrition, String inTranslate){
        this.word = inWord;
        this.transcription = inTranscrition;
        this.translate = inTranslate;
    }

    public void setWord(String str){
        this.word = str;
    }

    public void setTranscription(String str){
        this.transcription = str;
    }

    public void setTranslate(String str){
        this.translate = str;
    }

    public String getWord (){
        return word;
    }

    public String getTranscription(){
        return transcription;
    }

    public String getTranslate(){
        return translate;
    }
}
