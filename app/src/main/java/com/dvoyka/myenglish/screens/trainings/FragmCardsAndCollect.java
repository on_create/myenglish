package com.dvoyka.myenglish.screens.trainings;

import android.content.ClipData;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dvoyka.myenglish.FirstActivity.Training;
import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.db.BaseDB;
import com.dvoyka.myenglish.db.DBHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class FragmCardsAndCollect extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {

    private LinearLayout ll1, ll2;
    private BaseDB db;
    private Button buttonCheck;

    private Button[] buttonChoise = new Button[4];
    private int[] ids = {R.id.buttonCardChoise1, R.id.buttonCardChoise2, R.id.buttonCardChoise3, R.id.buttonCardChoise4};

    private String currentWord;
    private ImageView imageView;
    private TextView textViewWord;
    private Training training;
    private ArrayList<Integer> randomNumbers;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_cards_and_collect, container, false);

        db = new BaseDB(getActivity());
        db.open();

        imageView = (ImageView) view.findViewById(R.id.imageViewImage);
        textViewWord = (TextView) view.findViewById(R.id.textViewWord);

        ll1 = (LinearLayout) view.findViewById(R.id.collect_contaner_word);
        ll2 = (LinearLayout) view.findViewById(R.id.collect_contaner_letters);
        ll1.setOnDragListener(new MyDragListener());
        ll2.setOnDragListener(new MyDragListener());

        training = (Training) getArguments().getSerializable(getString(R.string.training));

        showFragmentContent(training, view);

        getActivity().getSupportLoaderManager().initLoader(0, null, this);

        return view;
    }

    void showFragmentContent(Training training, View view){
        switch (training){
            case CARDS_RU:
                initButtons(view);
                break;
            case CARDS_EN:
                initButtons(view);
                break;
            case COLLECT:
                view.findViewById(R.id.container_collect).setVisibility(View.VISIBLE);
                buttonCheck = (Button) view.findViewById(R.id.buttonCheckCollect);
                buttonCheck.setOnClickListener(this);
                break;
        }
    }

    void initButtons(View view){
        view.findViewById(R.id.container_card).setVisibility(View.VISIBLE);
        for(int i = 0; i < buttonChoise.length; i++){
            buttonChoise[i] = (Button) view.findViewById(ids[i]);
            buttonChoise[i].setOnClickListener(this);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new DataLoader(getActivity(), db, training);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        data.moveToFirst();
        Random random = new Random();
        int numberRow = random.nextInt(data.getCount())+1;
        data.moveToPosition(numberRow);

        randomNumbers = new ArrayList<>();
        randomNumbers.add(numberRow);
        while(randomNumbers.size() < 4){
            numberRow = random.nextInt(data.getCount())+1;
            if(!randomNumbers.contains(numberRow))
                randomNumbers.add(numberRow);
        }
        Collections.shuffle(randomNumbers);

        switch (training){
            case CARDS_RU:
                currentWord = data.getString(data.getColumnIndex(DBHelper.WORD_RU));
                setHelpData(data.getString(data.getColumnIndex(DBHelper.WORD_EN)), null);
                passRandomWords(data, DBHelper.WORD_RU);
                break;
            case CARDS_EN:
                currentWord = data.getString(data.getColumnIndex(DBHelper.WORD_EN));
                setHelpData(data.getString(data.getColumnIndex(DBHelper.WORD_RU)), currentWord);
                passRandomWords(data, DBHelper.WORD_EN);
                break;
            case COLLECT:
                currentWord = data.getString(data.getColumnIndex(DBHelper.WORD_EN));
                takeApart(currentWord);
                setHelpData(data.getString(data.getColumnIndex(DBHelper.WORD_RU)), currentWord);
                break;
        }
    }

    void passRandomWords(Cursor cursor, String columnName){
        for(int i = 0; i < randomNumbers.size(); i++){
            cursor.moveToPosition(randomNumbers.get(i));
            buttonChoise[i].setText(cursor.getString(cursor.getColumnIndex(columnName)));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    void setHelpData(String word, String wordEng){
        if(wordEng == null)
            wordEng = word;
        textViewWord.setText(word);

        InputStream ims = null;
        try {
            ims = getActivity().getAssets().open("images/"+wordEng+".jpg");
            Drawable d = Drawable.createFromStream(ims, null);
            imageView.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void takeApart(String str){
        ArrayList<String> list = new ArrayList<>();
        for(int i = 0; str.length() > i; i++)
            list.add(String.valueOf(str.charAt(i)));

        Collections.shuffle(list);
        for(int i = 0; list.size() > i; i++){
            TextView textView = new TextView(getActivity());
            textView.setOnTouchListener(new MyTouchListener());
            textView.setText(list.get(i));
            textView.setTextSize(50);
            ll2.addView(textView);
        }
    }

    void checkWordCollect(){
        String word = "";
        if(ll1.getChildCount()>0)
            for(int i = 0; i < ll1.getChildCount(); i++){
                TextView textView = (TextView) ll1.getChildAt(i);
                word = word + textView.getText();
            }
        if(word.equals(currentWord)){
            Toast.makeText(getActivity(), "ok", Toast.LENGTH_LONG).show();
            ll1.removeAllViews();
            getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
        }
        else{
            ll1.removeAllViews();
            ll2.removeAllViews();
            getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
            Toast.makeText(getActivity(), "no", Toast.LENGTH_LONG).show();
        }

    }

    void checkWord(String choiseWord){
        if(choiseWord.equals(currentWord))
            Toast.makeText(getActivity(), "ok", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getActivity(), "no", Toast.LENGTH_LONG).show();
        getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonCheckCollect:
                checkWordCollect();
                break;
            case R.id.buttonCardChoise1:
                checkWord(buttonChoise[0].getText().toString());
                break;
            case R.id.buttonCardChoise2:
                checkWord(buttonChoise[1].getText().toString());
                break;
            case R.id.buttonCardChoise3:
                checkWord(buttonChoise[2].getText().toString());
                break;
            case R.id.buttonCardChoise4:
                checkWord(buttonChoise[3].getText().toString());
                break;
        }
    }

    static class DataLoader extends CursorLoader {
        BaseDB db;
        Training training;
        public DataLoader(Context context, BaseDB db, Training training) {
            super(context);
            this.db = db;
            this.training = training;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = null;
            switch (training){
                case CARDS_RU:
                    cursor = db.getRandomWordCardRu();
                    break;
                case CARDS_EN:
                    cursor = db.getRandomWordCardEn();
                    break;
                case COLLECT:
                    cursor = db.getRandomWordCollect();
                    break;
            }
            return cursor;
        }
    }

    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    container.addView(view);
                    view.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                default:
                    break;
            }
            return true;
        }
    }

}
