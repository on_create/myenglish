package com.dvoyka.myenglish.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.dialog.DialogAddDictionary;
import com.dvoyka.myenglish.dialog.DialogSelectDictionary;

public class FragmSettingDictionary extends Fragment implements View.OnClickListener {

    private TextView textViewChoiseDictionary, textViewAddDictionary, textViewStatistic;
    FragmentManager fm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_settings, container, false);

        fm = getActivity().getSupportFragmentManager();

        textViewChoiseDictionary = (TextView)view.findViewById(R.id.textViewChoiseDictionary);
        textViewChoiseDictionary.setOnClickListener(this);
        textViewAddDictionary = (TextView)view.findViewById(R.id.textViewAddDictionary);
        textViewAddDictionary.setOnClickListener(this);
        textViewStatistic = (TextView)view.findViewById(R.id.textViewStatistic);
        textViewStatistic.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.textViewChoiseDictionary:
                DialogSelectDictionary dFragmentSelect = new DialogSelectDictionary();
                dFragmentSelect.show( fm, getResources().getString(R.string.fragment_select_dictionary));
                break;
            case R.id.textViewAddDictionary:
                DialogAddDictionary dFragmentAdd = new DialogAddDictionary();
                dFragmentAdd.show( fm, getResources().getString(R.string.fragment_select_dictionary));
                break;
            case R.id.textViewStatistic:
                break;
        }
    }
}
