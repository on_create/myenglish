package com.dvoyka.myenglish.screens.games;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;

import com.dvoyka.myenglish.R;

public class SpriteMemory {

    private int color;
    private int indentFrame;
    private int indent;
    private int stepAnimUp, stepAnimDown;

    private Context mycontext;

    public SpriteMemory(Context context, int colorSprite, int indentF, int indent, int stepAnimUp, int stepAnimDown){
        this.mycontext = context;
        this.color = colorSprite;
        this.indentFrame = indentF;
        this.indent = indent;
        this.stepAnimUp = stepAnimUp;
        this.stepAnimDown = stepAnimDown;
    }


    public void setColorSprite (int colorSprite){
        this.color = colorSprite;
    }

    public int getColorSprite(){
        return color;
    }

    public void setStepAnimUp(int step){
        this.stepAnimUp = step;
    }

    public int getStepAnimUp(){
        return stepAnimUp;
    }

    public void setStepAnimDown(int step){
        this.stepAnimDown = step;
    }

    public int getStepAnimDown(){
        return stepAnimDown;
    }

    public void draw(Canvas canvas, Paint p, int weight, int x, int y, int stepAnim) {
        p.setColor(color);
        p.setShadowLayer(5.0f, 1.0f, 1.0f, ContextCompat.getColor(mycontext, R.color.colorShadow));
        canvas.drawRoundRect(new RectF((x + stepAnim) + indentFrame, y + stepAnim + indent, (x + weight - stepAnim) + indentFrame, y + weight - stepAnim + indent), stepAnim, stepAnim, p);
    }
}
