package com.dvoyka.myenglish.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.screens.dictionarys.Word;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class DictionaryListAdapter extends ArrayAdapter<Word> {

    private int layoutResourceId;
    private ArrayList<Word> wordArrayList;
    private static LayoutInflater inflater = null;
    private DisplayImageOptions options;
    private Typeface tf;
    private Context ctx;
    private final String ASSETS = "assets://images/", JPG = ".jpg", RAW = "raw";

    private MediaPlayer mediaPlayer;

    public DictionaryListAdapter(Context context, int resource, ArrayList<Word> list) {
        super(context, resource, list);
        this.layoutResourceId = resource;
        this.wordArrayList = list;
        this.ctx = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        tf = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.fonts_luc));

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.empty_photo)
                .showImageOnFail(R.drawable.empty_photo)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static class ViewHolder {
        public TextView tv_word;
        public TextView tv_transcription;
        public TextView tv_translate;
        public ImageView imageView;
        public ImageView imageViewPlay;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                view = inflater.inflate(layoutResourceId, null);
                holder = new ViewHolder();

                holder.tv_word = (TextView) view.findViewById(R.id.item_dictionary_word);
                holder.tv_transcription = (TextView) view.findViewById(R.id.item_dictionary_transcription);
                holder.tv_transcription.setTypeface(tf);
                holder.tv_translate = (TextView) view.findViewById(R.id.item_dictionary_translate);
                holder.imageView = (ImageView) view.findViewById(R.id.imageViewItemDictionary);
                holder.imageViewPlay = (ImageView) view.findViewById(R.id.imageViewPlayDictionary);


                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.tv_word.setText(wordArrayList.get(position).getWord());
            holder.tv_transcription.setText(wordArrayList.get(position).getTranscription());
            holder.tv_translate.setText(wordArrayList.get(position).getTranslate());

            holder.imageViewPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playWord(wordArrayList.get(position).getWord());
                }
            });

            String imageUri = ASSETS + wordArrayList.get(position).getWord() + JPG;

            ImageLoader.getInstance().displayImage(imageUri, holder.imageView, options);

        } catch (Exception e) {}

        return view;
    }

    private void playWord(String name){
        mediaPlayer = MediaPlayer.create(ctx, ctx.getResources().getIdentifier(name, RAW, ctx.getPackageName()));
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(onCompletionListener);
    }

    MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.release();
        }
    };
}
