package com.dvoyka.myenglish.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.screens.dictionarys.FragmVerbsItem;
import com.dvoyka.myenglish.screens.dictionarys.Verb;

import java.util.ArrayList;

public class VerbsPagerAdapter extends FragmentPagerAdapter {

    Context context;
    ArrayList<Verb> list;

    public VerbsPagerAdapter(FragmentManager fm, Context context, ArrayList<Verb> list) {
        super(fm);
        this.context = context;
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle arguments = new Bundle();
        arguments.putString(context.getString(R.string.word_inf), list.get(position).getWordI());
        arguments.putString(context.getString(R.string.transcription_inf), list.get(position).getTranscriptionI());
        arguments.putString(context.getString(R.string.word_ps), list.get(position).getWordPS());
        arguments.putString(context.getString(R.string.transcription_ps), list.get(position).getTranscriptionPS());
        arguments.putString(context.getString(R.string.word_pp), list.get(position).getWordPP());
        arguments.putString(context.getString(R.string.transcription_pp), list.get(position).getTranscriptionPP());
        arguments.putString(context.getString(R.string.translate), list.get(position).getTranslate());

        FragmVerbsItem fragment = new FragmVerbsItem();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
