package com.dvoyka.myenglish.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dvoyka.myenglish.R;
import com.dvoyka.myenglish.screens.dictionarys.FragmDictionarysContent;

import java.io.Serializable;
import java.util.ArrayList;

public class DictionaryPagerAdapter extends FragmentPagerAdapter {

    private final String[] dictNames;
    private ArrayList<ArrayList> listData = new ArrayList<>();
    private Resources res;

    public DictionaryPagerAdapter(FragmentManager fm, Context context, ArrayList<ArrayList> list) {
        super(fm);
        res = context.getResources();
        dictNames = res.getStringArray(R.array.namecategory);
        listData = list;
    }

    @Override
    public Fragment getItem(int idx) {
        // TODO Auto-generated method stub

        Bundle arguments = new Bundle();
        arguments.putSerializable(res.getString(R.string.array_object), (Serializable)listData.get(idx));
        FragmDictionarysContent dictionarysContent = new FragmDictionarysContent();
        dictionarysContent.setArguments(arguments);

        return dictionarysContent;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return dictNames.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return dictNames[position];
    }

}
