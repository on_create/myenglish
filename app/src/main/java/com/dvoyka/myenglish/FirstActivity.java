package com.dvoyka.myenglish;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dvoyka.myenglish.screens.FragmSettingDictionary;
import com.dvoyka.myenglish.screens.dictionarys.FragmVerbs;
import com.dvoyka.myenglish.screens.trainings.FragmCardsAndCollect;
import com.dvoyka.myenglish.screens.games.FragmMemoryAndSpeed;
import com.dvoyka.myenglish.screens.dictionarys.FragmDictionarys;

public class FirstActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FragmDictionarys fragmDictionarys;
    private FragmVerbs fragmVerbs;
    private FragmCardsAndCollect fragmCardsAndCollect;
    private FragmMemoryAndSpeed fragmMemoryAndSpeed;
    private FragmSettingDictionary fragmSettingDictionary;
    private FragmentTransaction fTrans;
    private final int FILE_CODE = 1212;
    public enum Training {CARDS_RU, CARDS_EN, COLLECT}
    public enum Game {MEMORY, SPEED}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragmDictionarys = new FragmDictionarys();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.contaner_fragment, fragmDictionarys);
        fTrans.commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.first, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.nav_dictionarys:
                fragmDictionarys = new FragmDictionarys();
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.contaner_fragment, fragmDictionarys);
                fTrans.commit();
                break;
            case R.id.nav_phrases:

                break;
            case R.id.nav_irregular_verbs:
                fragmVerbs = new FragmVerbs();
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.contaner_fragment, fragmVerbs);
                fTrans.commit();
                break;
            case R.id.nav_traning01:
                goToTraining(Training.CARDS_RU);
                break;
            case R.id.nav_traning02:
                goToTraining(Training.CARDS_EN);
                break;
            case R.id.nav_traning03:
                goToTraining(Training.COLLECT);
                break;
            case R.id.nav_traning04:
                goToGame(Game.MEMORY);
                break;
            case R.id.nav_traning05:
                goToGame(Game.SPEED);
                break;
            case R.id.nav_settings:
                fragmSettingDictionary = new FragmSettingDictionary();
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.contaner_fragment, fragmSettingDictionary);
                fTrans.commit();
                break;
            case R.id.nav_send:
                break;
            case R.id.nav_share:
                break;
            case R.id.nav_estimate:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void goToTraining(Training name){
        fragmCardsAndCollect = new FragmCardsAndCollect();
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.training), name);
        fragmCardsAndCollect.setArguments(bundle);
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.contaner_fragment, fragmCardsAndCollect);
        fTrans.commit();
    }

    private void goToGame(Game name){
        fragmMemoryAndSpeed = new FragmMemoryAndSpeed();
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.training), name);
        fragmMemoryAndSpeed.setArguments(bundle);
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.contaner_fragment, fragmMemoryAndSpeed);
        fTrans.commit();
    }
}
